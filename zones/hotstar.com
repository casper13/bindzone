zone "www.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "api.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "service.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "services.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "secure-getcdn.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "geoip.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hssportsprepack.akamaized.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "live-news.akt.hotstar-cdn.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hesads.akamaized.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

//media.hotstar.com
//160.153.137.153

zone "hses.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses0.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses1.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses2.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses3.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses4.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses5.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses6.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses7.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses8.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hses9.hotstar.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// Update live09p.hotstar.com for live00p.hotstar.com - live09p.hotstar.com
// Update live99p.hotstar.com for live0p.hotstar.com - live99p.hotstar.com

// Update live99.hotstar.com for live00.hotstar.com - live99.hotstar.com
// Update live999.hotstar.com for live000.hotstar.com - live999.hotstar.com
