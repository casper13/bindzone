//
// Domain only
//


//zone "www.hbo.com" {
//    type forward;
//    forward only;
//    forwarders {
//        167.71.75.238;
//        167.71.131.75;
//    };
//};
zone "hbogo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hbonow.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "hbomax.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "oauth.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "oauth-us.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "sessions.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "sessions-us.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "gateway.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "gateway-us.api.hbo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
