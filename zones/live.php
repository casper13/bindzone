<?php


//live00p.hotstar.com - live09p.hotstar.com
$file = 'live09p.hotstar.com';

file_put_contents($file, '//live00p.hotstar.com - live09p.hotstar.com');
for ($index = 0; $index < 10; $index++) {
    $sub = str_pad($index, 2, '0', STR_PAD_LEFT);
    $domain = 'live'.$sub.'p.hotstar.com';

    $template = '
zone "'.$domain.'" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};';
    file_put_contents($file, $template, FILE_APPEND);
}

//live0p.hotstar.com - live99p.hotstar.com
$file = 'live99p.hotstar.com';

file_put_contents($file, '//live0p.hotstar.com - live99p.hotstar.com');
for ($index = 0; $index < 100; $index++) {
    $sub = $index;
    $domain = 'live'.$sub.'p.hotstar.com';

    $template = '
zone "'.$domain.'" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};';
    file_put_contents($file, $template, FILE_APPEND);
}

//live00.hotstar.com - live99.hotstar.com
$file = 'live99.hotstar.com';

file_put_contents($file, '//live00.hotstar.com - live99.hotstar.com');
for ($index = 0; $index < 100; $index++) {
    $sub = str_pad($index, 2, '0', STR_PAD_LEFT);
    $domain = 'live'.$sub.'.hotstar.com';

    $template = '
zone "'.$domain.'" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};';
    file_put_contents($file, $template, FILE_APPEND);
}

//live000.hotstar.com - live999.hotstar.com
$file = 'live999.hotstar.com';

file_put_contents($file, '//live000.hotstar.com - live999.hotstar.com');
for ($index = 0; $index < 1000; $index++) {
    $sub = str_pad($index, 3, '0', STR_PAD_LEFT);
    $domain = 'live'.$sub.'.hotstar.com';

    $template = '
zone "'.$domain.'" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};';
    file_put_contents($file, $template, FILE_APPEND);
}
