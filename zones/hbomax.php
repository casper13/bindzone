<?php

$cnds = ['akm', 'lv3', 'lln', 'cf', ];

$file = 'hbo.com.wildcard.temp';

file_put_contents($file, '');

foreach ($cnds as $cdn) {
    file_put_contents(
        $file,
        '

//
// '. $cdn .'
//'.PHP_EOL,
        FILE_APPEND
    );
    for ($index = 0; $index < 100; $index++) {
        $sub = str_pad($index, 2, '0', STR_PAD_LEFT);
        // *.pro[10-99].[akm,lv3,lln,cf].cdn.hbomax.com
        $domain = 'pro'.$sub.'.'.$cdn.'.cdn.hbomax.com';

        $template = '
zone "'.$domain.'" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};';
        file_put_contents($file, $template, FILE_APPEND);
    }
}
