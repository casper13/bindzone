//
// Domain only
//
zone "di.rlcdn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};