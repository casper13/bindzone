zone "myip.opendns.com" {
    type forward;
    forward only;
    forwarders {
        208.67.222.222;
        208.67.220.220;
    };
};
