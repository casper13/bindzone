// - primevideo.com
// - *.primevideo.com
// + www.primevideo.com
zone "primevideo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// - amazonvideo.com
// - www.amazonvideo.com
// - *.amazonvideo.com
zone "amazonvideo.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// + music.amazon.com
// + www.music.amazon.com
// + *.music.amazon.com
zone "music.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// - media-amazon.com
// - www.media-amazon.com
// - *.media-amazon.com
zone "media-amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ext.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ext-eu.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ext-fe.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ps.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ps-fe.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ps-eu.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ps-eu.amazon.co.uk" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "atv-ps-fe.amazon.co.jp" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "fls-eu.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "fls-eu.amazon.co.uk" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "fls-na.amazon.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "aiv-cdn.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "pv-cdn.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "llnwd.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "video.a2z.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
