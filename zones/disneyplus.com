//www.disney.com	184.25.51.113:443

//www.disneyplus.com	195.181.168.135:443

//appconfigs.disney-plus.net. 198.55.97.57

//cdn.registerdisney.go.com	198.55.97.58:443
//tredir.go.com	138.199.2.71:443

//di-dtaectolog-us-prod-1.appspot.com	89.187.167.167:443

//bam-sdk-configs.bamgrid.com	195.181.168.131
//global.edge.bamgrid.com	195.181.168.137
//content.global.edge.bamgrid.com	195.181.168.137:443

//search-api-disney.svcs.dssott.com	195.181.168.133
//vod-bgc-na-east-1.media.dssott.com	195.181.168.133:443
//vod-ftc-na-west-1.media.dssott.com	195.181.168.133:443
//vod-akc-na-west-1.media.dssott.com	195.181.168.137:443
//vod-l3c-na-west-1.media.dssott.com	195.181.168.131:443
//vod-vzc-na-west-1.media.dssott.com	195.181.168.135:443
//vod-bgc-na-east-1.media.dssott.com	195.181.168.135:443

zone "disney.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "disneyjunior.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "disneyplus.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "disney-plus.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "dssott.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "uplynk.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "bamgrid.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "cdn.registerdisney.go.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "watchdisneyfe.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "disney.co.jp" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// zone "go.com" { type master; file "/etc/bind/bindzone/db/db.getflix.com.au"; };
