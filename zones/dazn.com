zone "www.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "tv.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "watch.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "feeds.api.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "startup-prod.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "favourites-prod.api.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

//zone "my.dazn.com" {
//    type forward;
//    forward only;
//    forwarders {
//        167.71.75.238;
//        167.71.131.75;
//    };
//};

zone "isl.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "isl-jp.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "isl-us.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "isl-ca.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "isl-eu.dazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "my-account-prod.ar.indazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "config-www.fe.indazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "startup.core.indazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "api.playback.indazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "concurrency.playback.indazn.com" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

zone "d151l6v8er5bdm.cloudfront.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// video stream

// secure.footprint.net
zone "dc1-voddash-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dc2-voddash-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dc1-vodhls-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dc2-vodhls-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dca-livedash-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dcb-livedash-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dca-livehls-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dcb-livehls-perform.secure.footprint.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// s.llnwi.net
zone "dc1-llca-voddazn-dznvodca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dc2-llca-voddazn-dznvodca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dca-llca-voddazn-dznvodca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dcb-llca-voddazn-dznvodca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dca-llca-livedazn-dznliveca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};
zone "dcb-llca-livedazn-dznliveca.s.llnwi.net" {
    type forward;
    forward only;
    forwarders {
        167.71.75.238;
        167.71.131.75;
    };
};

// akamaized.net
//zone "dc1live1203dazn.akamaized.net" {
//    type forward;
//    forward only;
//    forwarders {
//        167.71.75.238;
//        167.71.131.75;
//    };
//};
