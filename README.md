# Stream DNS settings

## Install BIND9 and git 

```bash

sudo apt update && sudo apt install -y bind9 git

```

## Clone bindzone repo

```bash

git -C /etc/bind clone https://casper13@bitbucket.org/casper13/bindzone.git

# chown -R bind:root /etc/bind/bindzone

```
### Setup bind system config

Create bind log dir

```bash

sudo mkdir -p /var/log/named && sudo chown bind: /var/log/named

```

Create log rotate bind config

```bash

echo '
/var/log/named/*.log
{
        rotate 7
        daily
        missingok
        notifempty
        create 0640 bind adm
        delaycompress
        compress
        copytruncate
}
' | sudo tee /etc/logrotate.d/named > /dev/null

```

```bash

echo '
logging {
//    channel default_file {
//        file "/var/log/named/default.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel general_file {
//        file "/var/log/named/general.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel database_file {
//        file "/var/log/named/database.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel security_file {
//        file "/var/log/named/security.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel config_file {
//        file "/var/log/named/config.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel resolver_file {
//        file "/var/log/named/resolver.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel xfer-in_file {
//        file "/var/log/named/xfer-in.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel xfer-out_file {
//        file "/var/log/named/xfer-out.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel notify_file {
//        file "/var/log/named/notify.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel client_file {
//        file "/var/log/named/client.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel unmatched_file {
//        file "/var/log/named/unmatched.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel queries_file {
//        file "/var/log/named/queries.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel network_file {
//        file "/var/log/named/network.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel update_file {
//        file "/var/log/named/update.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel dispatch_file {
//        file "/var/log/named/dispatch.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel dnssec_file {
//        file "/var/log/named/dnssec.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//    channel lame-servers_file {
//        file "/var/log/named/lame-servers.log" versions 3 size 5m;
//        severity dynamic;
//        print-time yes;
//    };
//
    category default { null; };
//    category default { default_file; };
//    category general { general_file; };
//    category database { database_file; };
//    category security { security_file; };
//    category config { config_file; };
//    category resolver { resolver_file; };
//    category xfer-in { xfer-in_file; };
//    category xfer-out { xfer-out_file; };
//    category notify { notify_file; };
//    category client { client_file; };
//    category unmatched { unmatched_file; };
//    category queries { queries_file; };
//    category network { network_file; };
//    category update { update_file; };
//    category dispatch { dispatch_file; };
//    category dnssec { dnssec_file; };
//    category lame-servers { lame-servers_file; };
};
' | sudo tee /etc/bind/named.conf.logging > /dev/null

```

### Setup bind DNS configs

```bash

echo '
acl goodclients {
    10.0.0.0/8;
    localhost;
    localnets;
};

options {
    allow-query { goodclients; };

    directory "/var/cache/bind";

    forwarders {
        #1.1.1.1;
        #1.0.0.1;
        #8.8.8.8;
        #8.8.4.4;
    };
    #recursion yes;
    #forward only;

    auth-nxdomain no;

    dnssec-validation auto;

    listen-on-v6 { any; };
    version "dnsmasq";
    // max-cache-size 50%;
};

include "/etc/bind/named.conf.logging";
include "/etc/bind/bindzone/named.conf.stream-zones";

' | sudo tee /etc/bind/named.conf.options > /dev/null

```


### Check bind configs and restart bind

```bash

sudo named-checkconf && sudo service bind9 restart

```

### Create deploy alias
```bash

echo '
alias deploy-hard="git -C /etc/bind/bindzone reset --hard HEAD && git -C /etc/bind/bindzone clean -f -d && git -C /etc/bind/bindzone pull && named-checkconf && sudo rndc reload"
alias deploy="git -C /etc/bind/bindzone pull && named-checkconf && sudo rndc reload"
' | sudo tee -a /root/.bash_aliases > /dev/null

```

### Create deploy bash script

```bash

echo '
#!/usr/bin/env bash
# bomb on any error
set -e
#git -C /etc/bind/bindzone reset --hard HEAD
#git -C /etc/bind/bindzone clean -f -d
git -C /etc/bind/bindzone pull && named-checkconf && rndc reload
' | sudo tee -a /root/deploy.sh > /dev/null 

chmod +x /root/deploy.sh

```


### Check DNS setting 

```bash

nslookup netflix.com 127.0.0.1

```

# Update installed already bind

```bash
echo '
#!/usr/bin/env bash
# bomb on any error
set -e
#git -C /etc/bind/bindzone reset --hard HEAD
#git -C /etc/bind/bindzone clean -f -d
git -C /etc/bind/bindzone pull && named-checkconf && rndc reload
' | sudo tee -a /root/deploy.sh > /dev/null && chmod +x /root/deploy.sh

echo '
alias deploy-hard="git -C /etc/bind/bindzone reset --hard HEAD && git -C /etc/bind/bindzone clean -f -d && git -C /etc/bind/bindzone pull && named-checkconf && sudo rndc reload"
alias deploy="git -C /etc/bind/bindzone pull && named-checkconf && sudo rndc reload"
' | sudo tee -a /root/.bash_aliases > /dev/null

git -C /etc/bind clone https://casper13@bitbucket.org/casper13/bindzone.git && chown -R bind:root /etc/bind/bindzone
echo 'include "/etc/bind/bindzone/named.conf.stream-zones";' | tee -a /etc/bind/named.conf.options > /dev/null
sudo named-checkconf && sudo service bind9 restart && nslookup netflix.com 127.0.0.1


```

# Deploy